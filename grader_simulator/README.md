# Grader

Simulate a simple grader in `Gazebo` and `Rviz`

## Install

```bash
# Move to catkin_ws
cd ~/catkin_ws/
source ./devel/setup.bash

# Build
cd <catkin_ws>/src
catkin_make
```

## Run

```bash
source ./devel/setup.bash

roslaunch grader_simulator display.launch model:='$(find grader_simulator)/urdf/grader.xacro'
```

## Dimensions
Dimensions are set in meters
- Length: 10.800
- Width: 3.220
- Wheelbase (length): 6.000
- Wheelbase (width): 2.600
- Wheel diameter: 1.490
- Wheel radius: 0.745
- Wheel width: 0.520
